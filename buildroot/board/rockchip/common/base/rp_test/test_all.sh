#!/bin/bash

sleep 4

echo "================ TF card test ==============="
TF_DISK=/mnt/sdcard/
if [ ! -f $TF_DISK ]; then
echo " _____ _____      ____    _    ____  ____     ___  _  __ "
echo "|_   _|  ___|    / ___|  / \  |  _ \|  _ \   / _ \| |/ / "
echo "  | | | |_ _____| |     / _ \ | |_) | | | | | | | | ' /  "
echo "  | | |  _|_____| |___ / ___ \|  _ <| |_| | | |_| | . \  "
echo "  |_| |_|        \____/_/   \_\_| \_\____/   \___/|_|\_\ "

else
       echo "NO TF"
	exit 1
fi

echo "                                              "
echo "================ Ethernet test ==============="
ifconfig wlan0 down
udhcpc eth0 >/dev/null  2>&1
sleep 1
ping -c 3 www.baidu.com >/dev/null  2>&1
    if [ $? -eq 0 ]
    then
echo " _____ _____ _   _ _____ ____  _   _ _____ _____    ___  _  __ "
echo "| ____|_   _| | | | ____|  _ \| \ | | ____|_   _|  / _ \| |/ / "
echo "|  _|   | | | |_| |  _| | |_) |  \| |  _|   | |   | | | | ' /  "
echo "| |___  | | |  _  | |___|  _ <| |\  | |___  | |   | |_| | . \  "
echo "|_____| |_| |_| |_|_____|_| \_\_| \_|_____| |_|    \___/|_|\_\ "

    else
        echo "Ethernet connect fail"
	exit 1
    fi


echo "                                          "
echo "================ Wifi test ==============="
echo "__        _____ _____ ___   _____ _____ ____ _____  "
echo "\ \      / /_ _|  ___|_ _| |_   _| ____/ ___|_   _| "
echo " \ \ /\ / / | || |_   | |    | | |  _| \___ \ | |   "
echo "  \ V  V /  | ||  _|  | |    | | | |___ ___) || |   "
echo "   \_/\_/  |___|_|   |___|   |_| |_____|____/ |_|   "
                                                  
ifconfig eth0 down
iw dev wlan0 scan | grep SSID


echo "                                          "
echo "================ Audio test ==============="
echo "Please listen music"
echo " __  __ _   _ ____ ___ ____  "
echo "|  \/  | | | / ___|_ _/ ___| "
echo "| |\/| | | | \___ \| | |     "
echo "| |  | | |_| |___) | | |___  "
echo "|_|  |_|\___/|____/___\____| "
                            
aplay /rp_test/doorbell.wav

echo "                                          "
echo "================ Uart test ==============="
echo " _   _   _    ____ _____   _____ _____ ____ _____  "
echo "| | | | / \  |  _ \_   _| |_   _| ____/ ___|_   _| "
echo "| | | |/ _ \ | |_) || |     | | |  _| \___ \ | |   "
echo "| |_| / ___ \|  _ < | |     | | | |___ ___) || |   "
echo " \___/_/   \_\_| \_\|_|     |_| |_____|____/ |_|   "
echo "test uart5"
timeout 4 ./rp_test/uart_test /dev/ttyS5
echo "test uart6"
timeout 4 ./rp_test/uart_test /dev/ttyS6

echo " "
echo "================= test finish==============="

